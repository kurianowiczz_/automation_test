import request from 'supertest';

describe('Create account test (e2e)', () => {
  const TEST_URL='https://dev.tracker-iq.com/api/acm/v1/signup/super';
  const requester = request(TEST_URL);

  // with cookie
  describe('POST', () => {
    it('should create new account', async () => {
      const createAccountData = { 
        account_name: 'Olex',
        name: 'Olex',
        email: 'olex@test.com',
        account_description: 'Olex test',
        account_company: 'test.company',
        account_site: 'https://olex.test.com',
        phone: '12345678',
       };
      
      const { status } = await requester
        .post('/')
        .set('cookie', 'cookie-example')
        .send(createAccountData);

      expect(status).toEqual(201);
    });
  });

  //without cookie
  describe('POST', () => {
    it('should throw an error', async () => {
      const createAccountData = { 
        account_name: 'Olex',
        name: 'Olex',
        email: 'olex@test.com',
        account_description: 'Olex test',
        account_company: 'test.company',
        account_site: 'https://olex.test.com',
        phone: '12345678',
       };

      const { status } = await requester
        .post('/')
        .send(createAccountData);

      expect(status).toEqual(401);
    });

    it('should throw an error', async () => {
      const createAccountData = {
        email: 'olex@test.com',
        phone: '12345678',
      };

      const { status } = await requester
      .post('/')
      .set('cookie', 'cookie-example')
      .send(createAccountData);

      expect(status).toEqual(400);
    });
  });
});
