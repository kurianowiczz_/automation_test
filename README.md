#### Test task for checking account creation.

Cases for testing successful creation (201), failed creation (400) and unauthorized request (401) were implemented.

In the project directory, you can run:

`npm i` - to install dependencies  
`npm run test` - to launch tests